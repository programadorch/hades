from crum import get_current_request  # para obtener la sesion actual del ususario
from django.shortcuts import redirect
from datetime import datetime
from django.contrib import messages
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


# -------super usuarios -----------------
class IsSuperuserMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        return redirect('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['date_now'] = datetime.now()
        return context


# ---------validacion de permisos----------
class ValidatePermissionRequiredMixin(object):
    # permisos que tienen las vistas de las categorias, clientes, usuarios etc
    permission_required = ''
    url_redirect = None

    # metodo que permite obtener los perimisos que llegaban de la vista yAPLICA PARA MAS DE 1 PERMISO POR VISTA
    def get_perms(self):
        # devuele un array de con todos los permisos que tenga esa vista
        perms = []
        # si es un estring
        if isinstance(self.permission_required, str):
            # envia el permiso
            perms.append(self.permission_required)
            # si no es un string lo convierte en una dupla
        else:
            perms = list(self.permission_required)
        return perms

    # metodo para redireccionar a una pagina en caso de que no tenga el permiso en este caso el dashboard
    def get_url_redirect(self):
        if self.url_redirect is None:
            return reverse_lazy('erp:dashboard')
        return self.url_redirect

    def dispatch(self, request, *args, **kwargs):
        # obtiene la variable de sesion actual del usuario
        request = get_current_request()
        # validacion de super usuario
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        # saber si existe esa vari
        # able ne la sesion del usuario
        if 'group' in request.session:
            # recuperando el grupo de la sesion
            group = request.session['group']
            # llamando todos los permisos de la vista
            perms = self.get_perms()
            # iterando los permisos para identificar si tiene 1 o mas permisos la vista
            for p in perms:
                # validando  la inexistencia de los permisos asosciados al grupo para  redirigir y mostrar mensaje de error
                if not group.permissions.filter(codename=p).exists():
                    messages.error(request, 'No tiene permiso para ingresar a este módulo')
                    return HttpResponseRedirect(self.get_url_redirect())
                    # si cumple con la validacion y existe 1 o varios permisos me enviara a la vista con return
            return super().dispatch(request, *args, **kwargs)
                    # si no tiene un usuario asociado tambien redirigira al dashboard                    
        messages.error(request, 'No tiene permiso para ingresar a este módulo')
        return HttpResponseRedirect(self.get_url_redirect())
