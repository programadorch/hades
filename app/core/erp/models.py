from datetime import datetime

from django.db import models
from django.forms import model_to_dict

from config.settings import MEDIA_URL, STATIC_URL
from core.erp.choices import gender_choices
from core.models import BaseModel


class Category(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nombre', unique=True)
    desc = models.CharField(max_length=500, null=True, blank=True, verbose_name='Descripción')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        ordering = ['id']


class Product(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nombre', unique=True)
    cat = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Categoría') #tiene una relacion
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    stock = models.IntegerField(default=0, verbose_name='Stock') #campo de stock de productos
    pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2, verbose_name='Precio de venta')

    def __str__(self):
        return self.name
    #combierte algunos campos que por default no se pueden manejar como diccionario de datos esos son los siguientes
    def toJSON(self):
        item = model_to_dict(self)
        item['full_name'] = '{} / {}'.format(self.name, self.cat.name) #envia la categoria y el nombre de producto concatenado
        item['cat'] = self.cat.toJSON()
        item['image'] = self.get_image()
        item['pvp'] = format(self.pvp, '.2f')
        return item

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'img/empty.png')

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering = ['id']


class Client(models.Model):
    names = models.CharField(max_length=150, verbose_name='Nombres')
    surnames = models.CharField(max_length=150, verbose_name='Apellidos')
    dni = models.CharField(max_length=10, unique=True, verbose_name='Dni')
    date_birthday = models.DateField(default=datetime.now, verbose_name='Fecha de nacimiento')
    address = models.CharField(max_length=150, null=True, blank=True, verbose_name='Dirección')
    gender = models.CharField(max_length=10, choices=gender_choices, default='male', verbose_name='Sexo')

    def __str__(self):
        # permite visualizar la representacion de los clientes con los datos concatenados, nombres apellidos, dni
        return self.get_full_name()
#----------Metodo get full name ----para  retornar los nombres concatenados del cliente ---------------------
    def get_full_name(self):
        return '{} {} / {}'.format(self.names, self.surnames, self.dni) #envia nombres apellidos y numero de cedula
#--------------------------------------------------------

    def toJSON(self):
        item = model_to_dict(self)
        item['gender'] = {'id': self.gender, 'name': self.get_gender_display()}
        item['date_birthday'] = self.date_birthday.strftime('%Y-%m-%d')
        # enviando los nombres completos del cliente a forms.js
        item['full_name'] = self.get_full_name()
        return item

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['id']


class Sale(models.Model):
    cli = models.ForeignKey(Client, on_delete=models.CASCADE) #tiene una relacion debemos usar Protec en vez de Cascade para evitar perder datos que se eliminan y generan perdida en cascada con otroas tablas de informaicon
    date_joined = models.DateField(default=datetime.now)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.cli.names
#el metodo json se usa para extraer los datos del modelo y poder mostrarlos en la vista 
    def toJSON(self):
        item = model_to_dict(self)
        item['cli'] = self.cli.toJSON()
        item['subtotal'] = format(self.subtotal, '.2f')
        item['iva'] = format(self.iva, '.2f')
        item['total'] = format(self.total, '.2f')
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        #recoorre todos los detalles que tiene la factura y los añade a mi colccionnde elementos , se puede visualizar en inspeccion
        item['det'] = [i.toJSON() for i in self.detsale_set.all()] #coloeccion de diccionarios, para acceder a la relacion
        return item
    #------Metodo para sobre escribir delete--------------------------------
    def delete(self, using=None, keep_parents=False):
        #iterando buscando las relaciones que tiene detasale con otras tablas
        for det in self.detsale_set.all():
            # hace que el stock se aumente con la cantidad de detalle cantidad a la hora de eliminar la venta
            det.prod.stock += det.cant
            #almacena la informacion
            det.prod.save()
            #se esta ejecutando la accion
        super(Sale, self).delete()

    #------Fin del  Metodo para sobre escribir delete------------------------

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'
        ordering = ['id']


class DetSale(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE) #tiene una relacion
    prod = models.ForeignKey(Product, on_delete=models.CASCADE) #tiene una relacion
    price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.prod.name
        #extrae la informacion de la BD a manera de diccionario para mostrarla por medio del controlador views a el hatml list
    def toJSON(self):
        item = model_to_dict(self, exclude=['sale']) #excluye la informacion de sale ya que tiene una relacion
        item['prod'] = self.prod.toJSON() #producto
        item['price'] = format(self.price, '.2f') #precio esta convertido a decimal para evitar errores
        item['subtotal'] = format(self.subtotal, '.2f') # subtotal
        return item

    class Meta:
        verbose_name = 'Detalle de Venta'
        verbose_name_plural = 'Detalle de Ventas'
        ordering = ['id']
