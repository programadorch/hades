var tblSale; /** variable que se iniciliaza cada vez que se carga el listado de la tabla lista de ventas **/
/** script propio de dattable- ver documentacion, esta funcion format presenta el contenido extra del despliegue del child row **/
function format(d) {
    console.log(d); /** muestra por consola si esta llegando la informacion**/
        /**Contenido html una tabla y un listado dentro para mostrar en el child row **/
    var html = '<table class="table">';
    html += '<thead class="thead-dark">'; /** en la documentacion de dattable estan los class="thead-dark" para personalizar estilos**/
    html += '<tr><th scope="col">Producto</th>';
    html += '<th scope="col">Categoría</th>';
    html += '<th scope="col">PVP</th>';
    html += '<th scope="col">Cantidad</th>';
    html += '<th scope="col">Subtotal</th></tr>';
    html += '</thead>';
    /** iterando con each el array det es donde esta ek detalle de los productos -iteracion por clave y valor del diccionario**/
    html += '<tbody>';
    $.each(d.det, function (key, value) {
        /** valores dinamicos **/
        html+='<tr>'
        html+='<td>'+value.prod.name+'</td>'
        html+='<td>'+value.prod.cat.name+'</td>'
        html+='<td>'+value.price+'</td>'
        html+='<td>'+value.cant+'</td>'
        html+='<td>'+value.subtotal+'</td>'
        html+='</tr>';
    });
    html += '</tbody>';
    return html;
}

$(function () {

    tblSale = $('#data').DataTable({
        //responsive: true,
        /** propiedades de datatable**/
        scrollX: true, /** propiedad que permite deslizar una barra espaciadora horizontal **/
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            /** parametros de los datos de la bd **/
            /**Codigo script de dattable para usar el child row **/
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "cli.names"},
            {"data": "date_joined"},
            {"data": "subtotal"},
            {"data": "iva"},
            {"data": "total"},
            {"data": "id"},
        ],
        columnDefs: [
            { /** Asignando signo pesos a el contenido de las columnas **/
                targets: [-2, -3, -4],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseFloat(data).toFixed(2);
                }
            },
            { /** Posicion de los botones borrar y consultar factura **/
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                     /** boton para borrar la factura **/
                    var buttons = '<a href="/erp/sale/delete/' + row.id + '/" class="btn btn-danger btn-xs btn-flat"><i class="fas fa-trash-alt"></i></a> ';
                     /** boton para editar la factura si quiere evitar editar la factura puede comentar el boton o asiganar permisos **/
                    buttons += '<a href="/erp/sale/update/' + row.id + '/" class="btn btn-warning btn-xs btn-flat"><i class="fas fa-edit"></i></a> ';
                    /** consultar factura   "details" etiqueta para llamarlo **/
                    buttons += '<a rel="details" class="btn btn-success btn-xs btn-flat"><i class="fas fa-search"></i></a> ';
                    /** Boton para visualizar la factura pdf **/
                     buttons += '<a href="/erp/sale/invoice/pdf/'+row.id+'/" target="_blank" class="btn btn-info btn-xs btn-flat"><i class="fas fa-file-pdf"></i></a> ';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
    /** ------llamar a la accion o evento al hacer click en el boton de ver el detalle de la venta (lupita)-----------**/
    $('#data tbody')
        .on('click', 'a[rel="details"]', function () {
            var tr = tblSale.cell($(this).closest('td, li')).index(); /**obtiene informacion al hacer click en la fila especifica **/
            var data = tblSale.row(tr.row).data(); /** trae el objeto que se esta seleccionando, metodo row para hallar la posicion del ojeto seleccionado **/
            console.log(data);
             /** inicilizacion de datatable a manera de ajax**/
            $('#tblDet').DataTable({
                responsive: true,
                autoWidth: false,
                destroy: true,
                deferRender: true,
                //data: data.det,
                ajax: {
                    url: window.location.pathname,
                    type: 'POST',
                    data: {
                         /** enviando parametros accion e id**/
                        'action': 'search_details_prod',
                        'id': data.id /** extrae el id de data el diccionario de datos alimentado por JSON**/
                    },
                    dataSrc: ""
                },
                columns: [
                     /** selecciona atroibutos dentro de data con los nombres de los datos del diccionario **/
                    {"data": "prod.name"},
                    {"data": "prod.cat.name"},
                    {"data": "price"},
                    {"data": "cant"},
                    {"data": "subtotal"},
                ],
                /**con esta propiedad en datatable puede mificar propiedades de las columnas  **/
                columnDefs: [
                    {/** posicion de las columnas a modificar **/
                        targets: [-1, -3],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return '$' + parseFloat(data).toFixed(2); /** asignacion de signo pesos y decimales**/
                        }
                    },
                    {
                        targets: [-2],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                ],
                initComplete: function (settings, json) {

                }
            });
            /** Muestra el modal o ventana emergente**/
            $('#myModelDet').modal('show');
        }) 
        /**-----codigo propio de dattable -------evento para desplegar detalles con el boton + de child rows--------doc datatable------------------- **/
        .on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tblSale.row(tr); /** tblSale es la variable que permite enlazar con mi tabla**/
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown'); /** format es el metodo de dattable que permite visualizar el despliegue**/
            }
        });

});