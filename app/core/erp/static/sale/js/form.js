var tblProducts;
var tblSearchProducts; //variable que representa al datatble del modal, se va alimentar cuando se traigan los datos del modal
/** Variable para identificar la tabla  principal de listado de ventas **/
var vents = {
    /**contener la estructura  de la cabecera **/
    items: {
        cli: '', /** cliente **/
        date_joined: '', /** fecha de registro**/
        subtotal: 0.00,
        iva: 0.00,
        total: 0.00,
        products: [] /** productos en el array **/
    }, /** -----------funcion para calcular el valor de la venta ------------------- **/
    //--funcion dentro de la variable vents que devuelve  o captura los id de los prductos seleccionados en las ventas------------------
    //---para enviarlos a otra funcion y validar ecitando la repeticion de los productos en el listado
    get_ids: function () {
        var ids = [];
        //recorrido de los productos
        $.each(this.items.products, function (key, value) {
            // agregando elemento
            ids.push(value.id);
        }); //retornando los id
        return ids;
    },
    //-----------------------------
    calculate_invoice: function () {
        var subtotal = 0.00;
        /** sumador para subtotal**/
        var iva = $('input[name="iva"]').val(); /** lo que tiene el imput uiva **/
        /** -------------OPERACIONES MATEMATICAS ITERADAS------------------------ **/
        /** recorrer los items de los productos moviendo la posicion en el diccionario**/
        $.each(this.items.products, function (pos, dict) {
            dict.subtotal = dict.cant * parseFloat(dict.pvp);
            /** OPERACION MATEMATICA cantidad * precio de venta **/
            subtotal += dict.subtotal; /** OPERACION MATEMATICA DE SUMAR TODOS LOS SUBTOTALES**/
        });
        /**------- OPERACIONES MATEMATICAS CON RESULTADOS DEFINITIVOS------------ **/
        this.items.subtotal = subtotal;
        /** asignando los datos a la variable subtotoal**/
        this.items.iva = this.items.subtotal * iva;
        /** operaion matematica  la suma de los subtotales * el valor del iva **/
        this.items.total = this.items.subtotal + this.items.iva; /** total igual a subtotal mas iva **/
        /**---------------- PRESENTANDO O MOSTRANDO RESULTADOS-----------------------**/
        $('input[name="subtotal"]').val(this.items.subtotal.toFixed(2));
        /** se presenta el el componente se la derecha subtotal**/
        $('input[name="ivacalc"]').val(this.items.iva.toFixed(2));
        /** iva final calculado  **/
        $('input[name="total"]').val(this.items.total.toFixed(2)); /** **/
    },

    /** --------------funcion para insertar y listas los productos busqcados --------------- **/
    add: function (item) {
        this.items.products.push(item);
        /** colcoando la informacion dentro de los items de productos **/
        this.list(); /** inserta la busqueda en la tabla de productos  **/
    },
    /** Listando o mostrando los datos de product en la tabla ventas usando datatable**/
    list: function () {
        /** tblProducts es el id de la tabla para mostrar productos seleccionadoe en la venta **/
        this.calculate_invoice();
        /** sirve para calcular la posicion del producto  **/
        tblProducts = $('#tblProducts').DataTable({
            /** asignando la variable  tblProducts a el datatable **/
            responsive: true,
            autoWidth: false,
            destroy: true,
            data: this.items.products,
            /** enviando la coleccion de diccionario **/
            columns: [
                /** Datos de la tabla **/
                {"data": "id"},
                {"data": "full_name"},
                {"data": "stock"},
                {"data": "pvp"},
                {"data": "cant"},
                {"data": "subtotal"},
            ],
            /** ------------ Personalizando el contenido de las columnas del formulario list --COMPONENTES---------------**/
            columnDefs: [

                {
                    targets: [-4], /** stock **/
                    class: 'text-center', /** texto centrado **/
                    orderable: false,
                    /** envindo el boton de eliminar productos de la lista (el boton es un obajeto html)**/
                    render: function (data, type, row) {
                        //validando el color de las cantidades de stock y mostrando mensaje emergente de alerta
                        //data trae el valor que se le esta asignando a la columna sttock

                        if (row.stock >= 20) {
                            return '<span class="badge badge-success">' + data + '</span>'
                        } else if (row.stock > 10) {
                            return '<span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="Te estas quedando posc@s ' + row.name + 's">' + data + '</span>';
                        }
                        return '<span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Debe Urgentemente comprar mas ' + row.name + 's">' + data + '</span>';


                    }
                }, {
                    targets: [0], /** primera Posicion de las columnas  para ubicar el boton de eliminar seleccion **/
                    class: 'text-center', /** texto centrado **/
                    orderable: false,
                    /** envindo el boton de eliminar productos de la lista (el boton es un obajeto html)**/
                    render: function (data, type, row) {
                        return '<a rel="remove" class="btn btn-danger btn-xs btn-flat" style="color: white;"><i class="fas fa-trash-alt"></i></a>';
                    }
                },
                {
                    /** -3 es la posicion de la columna  pvp o valor de venta de producto  **/
                    targets: [-3],
                    class: 'text-center',
                    orderable: false,
                    /** inserta un signo pesos en el valor de producto **/
                    render: function (data, type, row) {
                        /** convierte a float con 2 decimales **/
                        return '$' + parseFloat(data).toFixed(2);
                    }
                },
                {
                    /**posicion -2 es la de cantidad, aqui se vera un imput para colocar manualmente la cantidad **/
                    targets: [-2],
                    class: 'text-center',
                    orderable: false,
                    /** Inserta la caja de texto para escribir manualmente las cantidades **/
                    render: function (data, type, row) {
                        /**row.cant coloca como cantidad el numero 1 por defecto en la fila / input-sm recorta el ancho del input de cantidades con  touchspin**/
                        return '<input type="text" name="cant" class="form-control form-control-sm input-sm" autocomplete="off" value="' + row.cant + '">';
                    }
                },
                {
                    /** -1 es la posicion del subtotal **/
                    targets: [-1],
                    class: 'text-center',
                    orderable: false,
                    /** inserta el signo pesos y en floar con 2 decimales **/
                    render: function (data, type, row) {
                        return '$' + parseFloat(data).toFixed(2);
                    }
                },
            ],
            /** -----------------   agregando aumentador de cantidades con touchspin el mismo que usamos en el iva   ----------------------------**/
            rowCallback(row, data, displayNum, displayIndex, dataIndex) {
                /** hace una busqueda del componenete llamado cantidad para agregarle el touchspin **/
                $(row).find('input[name="cant"]').TouchSpin({
                    min: 1, /** cantidad minima **/
                    max: data.stock, /** Valida que la cantidad maxima para vender sea la del stock del producto**/
                    step: 1
                });

            },
            initComplete: function (settings, json) {

            }
        });
    },

}

/** funcion para mostrar imagenes de los productos en la busqueda con select 2 y la propiedad **/
function formatRepo(repo) {
    if (repo.loading) {
        return repo.text;
    }

    var option = $(
        '<div class="wrapper container">' +
        '<div class="row">' +
        '<div class="col-lg-1">' +
        '<img src="' + repo.image + '" class="img-fluid img-thumbnail d-block mx-auto rounded">' +
        '</div>' +
        '<div class="col-lg-11 text-left shadow-sm">' +
        //'<br>' +
        '<p style="margin-bottom: 0;">' +
        '<b>Nombre:</b> ' + repo.name + '<br>' +
        '<b>Categoría:</b> ' + repo.cat.name + '<br>' +
        '<b>PVP:</b> <span class="badge badge-warning">$' + repo.pvp + '</span>' +
        '</p>' +
        '</div>' +
        '</div>' +
        '</div>');

    return option;
}

/** --------funcion para los componenetes de sales --------- inicializador de select**/
$(function () {
    $('.select2').select2({
        /**selec2 es generico y sirve para desplegar listados se uso para ver clientes  **/
        theme: "bootstrap4",
        language: 'es'
    });
    /** Funcion para el calendario emergente de fechas **/
    $('#date_joined').datetimepicker({
        format: 'YYYY-MM-DD', /**formato de fecha **/
        date: moment().format("YYYY-MM-DD"), /**propiedad para poner en imput un a fceha por defecto **/
        locale: 'es', /** idioma español del calendario emergente**/
        //minDate: moment().format("YYYY-MM-DD") /** especifica topes de fechas  solo hasta cierto dia se puede seleccionar la fecha **/
    });
    /** Funcion para implementar el iva caja de texto incrementable con botones**/
    $("input[name='iva']").TouchSpin({
        /** especicifacion de rango minimo, maximo y el incremento, decimales **/
        min: 0, /**iva del 0  al 100 % **/
        max: 100,
        step: 0.01,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
        /** Recalcula al aumentar o disminuir el iva**/
        /** Pide que recalcule el valor de la factura gracias a change que es una propiedad de la libreria pikck en los votones de aumentar iva **/
    }).on('change', function () {
        vents.calculate_invoice();
    })
        .val(0.19); /** iva del 12 porciento **/

    // ------------------------------Buscar Clientes en el formulario de ventas ----------------------------------------------
    //cli es el componenete de busqueda de forms.py
    $('select[name="cli"]').select2({
        theme: "bootstrap4",
        language: 'es',
        allowClear: true,
        ajax: {
            delay: 250,
            type: 'POST',
            url: window.location.pathname,
            data: function (params) {
                var queryParameters = {
                    term: params.term,
                    //accion de buscar clientes
                    action: 'search_clients'
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
        },
        placeholder: 'Ingrese una descripción',
        minimumInputLength: 1,
    });
//------------------------------------Funcion para agregar clientes en el formulario de ventas --------
    //Presentacion o viaualizacion con show del modal con el id myModalClient del create.html
    $('.btnAddClient').on('click', function () {
        // se abre el modal apenas opima el bootn verde
        $('#myModalClient').modal('show');
    });
    //reinicia el contenido de datos del modal una vez halla almacenado o cancelado la escritura
    $('#myModalClient').on('hidden.bs.modal', function (e) {
        //limpia el formulario modal
        $('#frmClient').trigger('reset');
    })
    //----------validacion de campos del formulario para enviar la informacion con el boton guardar
    $('#frmClient').on('submit', function (e) {
        e.preventDefault();
        //creando una coleccion de elemntos con la informacion que se escribe en el formulario
        var parameters = new FormData(this);
        //enviando un accion para que llegue la data a la vista se debe crear esa accion dentro del metodo post
        parameters.append('action', 'create_client');
        // submit_with_ajax metodo de notificacion oara add cliente
        submit_with_ajax(window.location.pathname, 'Notificación',
            '¿Estas seguro de crear al siguiente cliente?', parameters, function (response) {
                //console.log(response);
                //-----fragmento de codigo de select2 para capturar la informacion del cliente en el modal-----selected true hace que apatrezca seleccionado los datos ------------
                var newOption = new Option(response.full_name, response.id, false, true);
                $('select[name="cli"]').append(newOption).trigger('change');
                //--------------------------
                $('#myModalClient').modal('hide');
            });
    });
    //-----------------------------------------------------------------------------------------------
    //------Funcion para autocompletar palabras e el buscador de productos-------------
    // search products
    // ---------------------autocompletado de palabras---------Buscar productos----------------------------------------------
    /** configuracion de Autocompletado de busquedas **/
    $('input[name="search"]').autocomplete({
        /** name="search" es el nombre del imput de buscar productos en create.html**/
        source: function (request, response) {
            /** -------------Validacion de errores --------------**/
            $.ajax({
                url: window.location.pathname, /**localizacion o direccionamiento  actual **/
                type: 'POST', /** tipo de  metodo **/
                data: {
                    'action': 'search_products',
                    'term': request.term, /** term es la variable que va a contener la busqueda  **/
                    'ids': JSON.stringify(vents.get_ids()) /** enviando los id a el autocomplete   **/
                },
                dataType: 'json',
            }).done(function (data) {
                response(data);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //alert(textStatus + ': ' + errorThrown);
            }).always(function (data) {
                /** Muestra el resultado de la busqueda satisfactoreamente **/
            });
        },
        /**Configuracion de la busqueda **/
        delay: 500,
        minLength: 1,
        select: function (event, ui) {
            event.preventDefault();
            /** detiene el evento para hacer la busqueda  **/
            console.clear();
            /** limpia las busquedas por consola de los datos **/
            ui.item.cant = 1;
            ui.item.subtotal = 0.00;
            console.log(vents.items);
            /** ver por consola los datos**/
            vents.add(ui.item);
            /** Se agrega el productos y automaticamente se enlista con la funcion add **/
            $(this).val(''); /** hace la  busqueda de datos **/
        }
    });
    //------fin de la funcion de autocompletador de palabaras en el buscador de productos
    /** Funcion para eliminar todo el listado de productos  **/
    $('.btnRemoveAll').on('click', function () {
        /** validacion  si los productos son 0 retorne a false y no salga el mensaje denuevo si esta vacia la lista **/
        if (vents.items.products.length === 0) return false;
        /** mensaje **/
        alert_action('Notificación', '¿Estas seguro de eliminar todos los items de tu detalle?', function () {
            vents.items.products = [];
            vents.list(); /** SE refresca el listado al eliminar **/
        });
    });
    // eventos dentro del listado de ventas
    $('#tblProducts tbody')
        /**evento de hacer clic en el boton remove borrar un producto especifico ,la etiqueta a de hetml que tenga nombre remove ahi sera el evento **/
        .on('click', 'a[rel="remove"]', function () {
            /** obtenemos la psicion del array a eliminar **/
            var tr = tblProducts.cell($(this).closest('td, li')).index();
            /** mensaje de alerta de eliminar 1 solo producto**/
            alert_action('Notificación', '¿Estas seguro de eliminar el producto de tu detalle?', function () {
                vents.items.products.splice(tr.row, 1);
                /** obtoene la posicion por filas identifica**/
                vents.list(); /** SE refresca el listado al eliminar **/
            });
        }) /** evento change actualiza los valores que escribamos en el componenete imput con name="cant" **/
        .on('change', 'input[name="cant"]', function () {
            console.clear();
            /** limpia el valor de la consola en inspeccionar**/
            var cant = parseInt($(this).val());
            /** recupera el valor escrito por teclado**/
            var tr = tblProducts.cell($(this).closest('td, li')).index();
            /** variable tblProducts contiene los valores de la tabla **/
            vents.items.products[tr.row].cant = cant;
            /**  [tr.row] es la posicion de los productos identifica el producto la fila para asignar o actualizar la cantidad  **/
            vents.calculate_invoice(); /**  metodo para recaluclar los valores de la factura cuando aumentamos las cantidades  **/
            /** actualizar los subtotales de los productos seleccionados / 5 es la posicion de la columna asiganar signos pesos e implantar el subtotal don 2 deciamles **/
            $('td:eq(5)', tblProducts.row(tr.row).node()).html('$' + vents.items.products[tr.row].subtotal.toFixed(2));
        });
    /** evento para cancelar la busqueda de productos**/
    $('.btnClearSearch').on('click', function () {
        $('input[name="search"]').val('').focus(); /** hace que el contenido actual del input se limpie en vacio y que se enfoque ahi focus() **/
    });
    //------EVENTO Mostrando los productos  el modal de busqueda de prodcutos con el id ------------------
    //--- despliega la ccion de dar click en buscar productos del listado
    // Programacion dela opcion  de busqueda de productos btnSearchProducts es la etiqueta que permite hacer la accion
    $('.btnSearchProducts').on('click', function () {
        tblSearchProducts = $('#tblSearchProducts').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            deferRender: true,
            ajax: {
                url: window.location.pathname,
                type: 'POST',
                data: {
                    // esta accion es muy importante para el envio de datos
                    'action': 'search_products',
                    'ids': JSON.stringify(vents.get_ids()),
                    /** enviando los id a el buscador de el modal para evitar mostrar productos repetidos en el listado de ventas   **/
                    // term es la variable de busqueda que figura en views, term se envia como parametro en la ccion search?products
                    'term': $('input[name="search"]').val()

                },
                dataSrc: ""
            },
            // datos que se van a mostrar en la tabla del modal busqueda de productos
            columns: [
                {"data": "full_name"}, //trae los datos concatenados desde el metodo tjon en el modelo
                {"data": "image"},
                {"data": "stock"},
                {"data": "pvp"},
                {"data": "id"},
            ],
            // personalizacion de las columnas del modal
            columnDefs: [
                {
                    targets: [-4],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<img src="' + data + '" class="img-fluid d-block mx-auto" style="width: 20px; height: 20px;">';
                    }
                },
                {
                    targets: [-3],
                    class: 'text-center',
                    render: function (data, type, row) {
                        if (row.stock >= 20) {
                            return '<span class="badge badge-success">' + data + '</span>'
                        } else if (row.stock > 10) {
                            return '<span class="badge badge-warning" data-toggle="tooltip" data-placement="top" title="Te estas quedando posc@s ' + row.name + 's">' + data + '</span>';
                        }
                        return '<span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Debe Urgentemente comprar mas ' + row.name + 's">' + data + '</span>';
                    }

                },
                {
                    // boton con el signo mas para agregar manualmente
                    targets: [-2],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        var buttons = '<a rel="add" class="btn btn-success btn-xs btn-flat"><i class="fas fa-plus"></i></a> ';
                        return buttons;
                    }
                },
            ],
            initComplete: function (settings, json) {

            }
        });
        //desplegando el modal
        $('#myModalSearchProducts').modal('show');
    });

    //----FIn del evento de mostrar el modal de productos-------------------------
    //-----Evento para el boton verde de agregar producto en el modal de busqueda de producto al listado de products---------
    $('#tblSearchProducts tbody')
        .on('click', 'a[rel="add"]', function () {
            //trayendo los datos de la variable gobal del modal
            var tr = tblSearchProducts.cell($(this).closest('td, li')).index();
            // trae los datos de productos y obetto completo
            var product = tblSearchProducts.row(tr.row).data();
            // enviando los datos del producto seleccionado a la variable de ventas
            product.cant = 1;
            product.subtotal = 0.00;
            // enviando el objeto al listado de venta
            vents.add(product);
            //codigo propio de datatable para remover filas de las tablas/ en este caso remueve productos que ya an sido seleccionados en el modal
            tblSearchProducts.row($(this).parents('tr')).remove().draw();
        });
    //--------fin del ---Evento para el boton verde de agregar producto en el modal-----------------
    /** evento para subir la informacion de los productos a la tabla sale de la base de datos**/
    $('#frmSale').on('submit', function (e) {
        e.preventDefault(); /** teniene el evento del submit para enviar los datos **/
        /**------ validacion para verificar que no se valla la informacion vacia --------------**/
        if (vents.items.products.length === 0) {
            message_error('Debe al menos tener un item en su detalle de venta');
            return false;
        }
        /** Envia el fecha de venta y el Id del cliente **/
        vents.items.date_joined = $('input[name="date_joined"]').val();
        vents.items.cli = $('select[name="cli"]').val();
        var parameters = new FormData();
        /** enviando parametros con atributo  parameters.append , verifica la recepcion del identificador action en el html**/
        /** ptregunta si existe una concidencia del nombre action en create.html y si existe extraer la informacion diligenciada en el input o caja de texto**/
        parameters.append('action', $('input[name="action"]').val());
        /** Envia la fecha de venta y el id del cleinte a la BD, JSON.stringify tranforma los datos a script json**/
        parameters.append('vents', JSON.stringify(vents.items));
        /** notificacion para imprimir la factura ?**/
        submit_with_ajax(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function (response) {
            /** Si la respuesta es si muestra el pdf para imprimir, si es no solo redirige a la vista**/
            /** Abre la factura pdf en otra pestaña aparte**/
            alert_action('Notificación', '¿Desea imprimir la boleta de venta?', function () {
                window.open('/erp/sale/invoice/pdf/' + response.id + '/', '_blank'); /** response.id es la variable que contiene el id de la venta o factura**/
                /** Despues de ver la factura redirige a listado de ventas **/

                /**cancel es el parametro de funtions.js global si canclamos nos redirige a listado de ventas **/ location.href = '/erp/sale/list/';
            }, function () {
                location.href = '/erp/sale/list/';
            });
        });
    });

//     /** Buscador de productos y autocomplete con libreria select2 , para ello comentamos el script de el anterior metodo que usamos **/
//     $('select[name="search"]').select2({
//         theme: "bootstrap4",
//         language: 'es',
//         allowClear: true,
//         ajax: {
//             delay: 250,
//             type: 'POST',
//             url: window.location.pathname,
//             /** parametros que le envia al metodo post**/
//             data: function (params) {
//                 var queryParameters = {
//                     term: params.term,
//                     action: 'search_products'
//                 }
//                 return queryParameters;
//             },
//             processResults: function (data) {
//                 return {
//                     results: data
//                 };
//             },
//         },
//         placeholder: 'Ingrese una descripción',
//         minimumInputLength: 1,
//         templateResult: formatRepo, /** propiedad que permite visualizar html de imagenes en miniatur para la busqueda de productos**/
//         /** est evento de select2 captura la data o  la seleccion del dato buscado  **/
//     }).on('select2:select', function (e) {
//         /** recupera la data capturada **/
//         var data = e.params.data;
//         /** pone la informacion recuperada en la tabla de venta **/
//         data.cant = 1;
//         data.subtotal = 0.00;
//         vents.add(data);
//         $(this).val('').trigger('change.select2'); /** limpia el contenido de busqueda**/
//     });
//
    vents.list();
//     // Esto se puso aqui para que funcione bien el editar y calcule bien los valores del iva. // sino tomaría el valor del iva de la base debe
//     // coger el que pusimos al inicializarlo.
});

