$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            // position es la variable que contiene ls numeraciones de la lista de productos
            {"data": "position"},
            {"data": "name"},
            {"data": "cat.name"},
            {"data": "image"},
            {"data": "stock"},
            {"data": "pvp"},
            {"data": "id"},
        ],
        columnDefs: [
            {
                targets: [-4],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '<img src="'+data+'" class="img-fluid d-block mx-auto" style="width: 20px; height: 20px;">';
                }
            },
            {
                targets: [-3],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    //validando el color de las cantidades de stock y mostrando mensaje emergente de alerta
                    //data trae el valor que se le esta asignando a la columna sttock
                      if (row.stock >= 20) {
                            return '<span class="badge badge-success">' + data + '</span>'
                        } else if (row.stock > 10) {
                            return '<span class="badge badge-warning" data-toggle="tooltip"  data-placement="top" title="Te estas quedando poc@s ' + row.name + 's">' + data + '</span>';
                        }
                        return '<span class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="Debe comprar Urgentemente mas  ' + row.name + 's">' + data + '</span>';
                    }
            },
            {
                targets: [-2],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$'+parseFloat(data).toFixed(2);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/erp/product/update/' + row.id + '/" class="btn btn-warning btn-xs btn-flat"><i class="fas fa-edit"></i></a> ';
                    buttons += '<a href="/erp/product/delete/' + row.id + '/" type="button" class="btn btn-danger btn-xs btn-flat"><i class="fas fa-trash-alt"></i></a>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});
