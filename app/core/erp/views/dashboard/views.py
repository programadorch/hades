#librerias para mostrar resultados totales de sumatorias
from random import randint #FUNCION PARA OBTENER NUMEROS ALEATORIOS
from django.contrib.auth.mixins import LoginRequiredMixin #metodo para validar logueo
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from datetime import datetime #libreria para obtener fecha actual
from core.erp.models import Sale #modelo de venta
from core.erp.models import Product, DetSale #IMPORTANDO LOS MODELOS A USAR
#vista del dashboard principal



class DashboardView(LoginRequiredMixin,TemplateView):
    template_name = 'dashboard.html'
    #--------sobreescritura del metodo post -------------------------
    @method_decorator(csrf_exempt) #deactiva el mecanismo de defensa de la vista para modificar el metodo post
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    #sobreescritura del metodo get para asignar por default el primer rol del usuario
    def get(self, request, *args, **kwargs):
        # ejecuta una sola vez la vista
        request.user.get_group_session()
        # no se altera el funcionamiento de la vista
        return super().get(request, *args, **kwargs)
    #sobreescribiendo metodo post
    def post(self, request, *args, **kwargs):
        data = {}
        try: #variable de verificacion para usar en template
            action = request.POST['action']
            # ------------------ACCION PARA ENVIAR DATOS AL TEMPLATE DE VENTAS MENSUALES PARA LOS GRAFICOS DE BARRAS
            if action == 'get_graph_sales_year_month':
                #PARAMETROS PARA ENVIAR A EL SCRIPT DE DASBOARD.HTML
                data = {
                    'name': 'Porcentaje de venta',
                    'showInLegend': False,
                    'colorByPoint': True,
                    'data': self.get_graph_sales_year_month() #ENVIANDO LA DATA CON LA FUNCION get_graph_sales_year_month
                }
                #------------------ACCION PARA ENVIAR DATOS AL TEMPLATE DE PRODUCTOS PARA LOS GRAFICOS DE PASTEL
            # ------------------ACCION PARA ENVIAR DATOS AL TEMPLATE DE PRODUCTOS VENDIDOS PARA EL GRAFICO DE PASTEL
            elif action == 'get_graph_sales_products_year_month':
                #PARAMETROS PARA VISUALIZAR EN MENSAJE EMERGENTE AL PASAR EL MOUSE
                data = {
                    'name': 'Porcentaje',
                    'colorByPoint': True,
                    'data': self.get_graph_sales_products_year_month(), # FUNCION DEL TEMPLATE get_graph_sales_products_year_month
                }
            # ------------------ACCION PARA ENVIAR DATOS AL TEMPLATE Y MOSTRAR GRAFICO ONLINE
            elif action == 'get_graph_online':
                #DENTRO DEL DICCIONARIO Y SE COLOCAN LOS VALORES ALEATORIOS
                data = {'y': randint(1, 100)} #NUMEROS ALEATORIOS DEL 1 AL 100
                print(data)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)
    #--------------metodo para  o traer  los datos para mostrar en el grafico--------------------------
    def get_graph_sales_year_month(self): #sel hace referencia a esta clase
        #------------ITERAR LAS VENTAS CALCULANDO LOS TOTALES POR MES Y POR AÑO -------------
        data = [] #contendra los valores de totales
        #si hay errores el try devuelve la data vacia
        try: #obteniendo año actual con libreria datetime
            year = datetime.now().year
            #iterar los 12 meses del año PARA CALCULAR CADA MES DE LAS VENTAS
            for m in range(1, 13):
                #filtro de mes y año / CONUSLTA DEL MODELO DE LA BD
                total = Sale.objects.filter(date_joined__year=year, date_joined__month=m).aggregate(
                    #caluclo de la sumatoria del total usando el metodo coalse
                    r=Coalesce(Sum('total'), 0)).get('r')
                #convierte la data a numeros
                data.append(float(total))
        except:
            pass
        return data #retoanndo ya termonado todo los vcalores se van a ir comulando
    #-------------------METODO PARA USAR PRODUCTOS EN EL GRAFICO DE PASTEL ----------------------
    def get_graph_sales_products_year_month(self):
        #PARAMETROS DE LA CONSULTA
        data = [] #DICCIONARIO DE DATOS
        year = datetime.now().year #OBTENIENDO EL AÑO
        month = datetime.now().month #OBTENIENDO EL MES
        #CONTROL DE ERRORES
        try: #ITERANDO LOS PRODUCTOS
            for p in Product.objects.all():
                #CONSULTA DEL MODELO DE  LA BD AÑO/MES Y PRODUCTO
                total = DetSale.objects.filter(sale__date_joined__year=year, sale__date_joined__month=month,
                                               prod_id=p.id).aggregate(
                    #SUMA EL SUBTOTAL
                    r=Coalesce(Sum('subtotal'), 0)).get('r')
                #VALIDACION PARA NO MOSTRAR PRODUCTOS EN EL PASQUEL QUE NO SE HAN VENDIDO SI > 0 GUARADE LA DATA SI NO PAILAS
                if total > 0:
                    #INCRUSTAR LOS VALORES EN UN DICCIONARIO -NOMBRE DE PROYUDCTO Y EL TOTAL O VALOR QUE TIENE
                    data.append({
                        'name': p.name,
                        'y': float(total) #CONVIRTIENDO A TIPO FLOAT EL VALOR DE LA VARIABLE
                    })
        except:
            pass
        return data #RETORNANDO LA DATA AL TEMPLATE

    #sobreescribiendo metodo para enviar parametros adicionales a los templates
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['panel'] = 'Panel de administrador'
        #enviando los valores de ventas al template con la siguiente vairable la cual usa la funcion  get_graph_sales_year_month()
        context['graph_sales_year_month'] = self.get_graph_sales_year_month() #self hace referencia ala clase actual usar el metodo de la clase actual
        return context
