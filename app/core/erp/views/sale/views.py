from django.contrib.auth.mixins import LoginRequiredMixin  # validador de logueo
from django.db import transaction  # libreria patra usar la funcion de no almacenar datos en falso
from django.db.models import Q # la usamos para poder hacer comparaciones con or |
from django.urls import reverse_lazy
from core.erp.forms import SaleForm, ClientForm  # importando el formulario
from core.erp.mixins import ValidatePermissionRequiredMixin  # validador de permisos
from django.utils.decorators import method_decorator
from django.views import View  # importacion de una vista sencilla
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, ListView, DeleteView, UpdateView  # vistas genericas de django
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect  # importando pat datos tipo JSON
import json
# importacion de modelos para almacenar datos en la BD
from core.erp.models import Sale, Client  # importando la bd sale del modelo venta
from core.erp.models import Product  # importando el modelo de base de datos product
from core.erp.models import DetSale  # importando bd det sale del modelo dellate de venta
# librerias para usar los reportes en pdf
import os
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
from xhtml2pdf import pisa


# clase para visualizar los productos y detalles ya almacenados en las BD
class SaleListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Sale
    template_name = 'sale/list.html'
    permission_required = 'view_sale'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Sale.objects.all():
                    data.append(i.toJSON())
            #
            elif action == 'search_details_prod':
                data = []  # esta variable se va llenando con los detalles de la venta extraido por el metodo toJSON dem modelo de detsale
                # hace un filtro si el id de la venta es el mismo qie el id del componenete seleccionado en el scrip de list.js
                for i in DetSale.objects.filter(sale_id=request.POST['id']):
                    data.append(i.toJSON())  # el data se va llenando con el diccionario del modelo de detsale
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Ventas'
        context['create_url'] = reverse_lazy('erp:sale_create')
        context['list_url'] = reverse_lazy('erp:sale_list')
        context['entity'] = 'Ventas'
        return context
# clase para mostrar los productos y detalles de la venta en pantalla
class SaleCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Sale  # modelo o base de datos
    form_class = SaleForm  # especificando el formulario con el que voy a mostrar
    template_name = 'sale/create.html'  # tempalte o maquetacion html para mostrar
    # al cancelar el listado de la venta redirige a listado de ventas
    success_url = reverse_lazy('erp:sale_list')  # obtiene la ruta absoluta cuando es exitosa la creacion de la venta
    permission_required = 'add_sale'
    url_redirect = success_url

    @method_decorator(csrf_exempt)  # decorador para poder hacer modificaciones al metodo post en django
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # metodo post para hacer el envio de informacion
    def post(self, request, *args, **kwargs):
        #print(request.POST)
        data = {}
        try:
            #acciones
            action = request.POST['action']
            if action == 'search_products':  # la accion es busqueda de productos
                data = []  # data es un array
                #conviertes los id de productos seleccionados de string a un listado
                ids_exclude = json.loads(request.POST['ids'])
                #lOGICA DE LA BUSQUEDA CON IMPUT Y MODAL / SI TERM TIENE TEXTO EL MODAL MUIESTRA PRODUCTOS CON ESE TEXTO SI NO EL MODAL MUESTRA TODOS LOS PRODUCTOS EXISTENTES
                # valores que llegan de forms.js
                term = request.POST['term'].strip()
                # Validacion de que el stock sea mayor a 0
                products = Product.objects.filter(stock__gt=0)
                # Validacion para filtrar por term - Si la busquea contiene informacion entonces
                if len(term):
                    # realiza la bsuqueda  si term contiene un texto de busqueda por el filtro de la informacion term
                    #solo muestra productos que estan en stock
                    products = products.filter(name__icontains=term, stock__gt=0)
                #si llega bacio term entonces  va a recorrer toda la informacion de productos
                #toma los productos identifica en la buqueda principal de productos en ventas los id y excluiye los productos que coinciden con el id almacenado en el listado ids_exclude
                for i in products.exclude(id__in=ids_exclude):
                    item = i.toJSON()
                    item['value'] = i.name
                    # item['text'] = i.name
                    data.append(item)
            elif action == 'add': #accion de agregar # ----bloque para la accion de almacenar el listado de productos  y detalle de venta en las 2 tablas de la BD---------------------------
                # transaction.atomic(): es funcion de django ejecuta todo y si hay un error ya no guarda lo que estaba procesando
                with transaction.atomic():
                    vents = json.loads(request.POST['vents'])  # recuperar el valor de fornm.js para datos del cliente
                    # inserciones de datos en la BD datos de la venta  son loa valores y datos del cleinte, tabla derecha
                    sale = Sale()  # venta
                    sale.date_joined = vents['date_joined']  # fecha de venta
                    sale.cli_id = vents['cli']  # id de cliente
                    sale.subtotal = float(vents['subtotal'])
                    sale.iva = float(vents['iva'])
                    sale.total = float(vents['total'])
                    sale.save()  # guarda los datos de valores de venta  en la BD sale
                    # iterando los productos
                    for i in vents['products']:
                        # llenando la bd la tabla detalle  de venta que son los productos tabla izquierda
                        det = DetSale()
                        det.sale_id = sale.id  # relacion dellate de venta  con id de la venta
                        det.prod_id = i['id']  # relacion con el id del producto
                        det.cant = int(i['cant'])  # convierte a entero y es la cantidad
                        det.price = float(i['pvp'])  # precio de venta pvp es la variable que lo recibe
                        det.subtotal = float(i['subtotal'])
                        det.save()  # guarda los datos de los productos en la  tabla detsale
                        #haciendo el descuento de productos del stock a la hora de vender
                        det.prod.stock -= det.cant #el stock va a hacer menor o igual a la cantidad que vendio en su momento
                        #almacena ese cambio en la tabla
                        det.prod.save()

                        data = {'id': sale.id} # diccionario que envia con Jresponse la clave primaria de la factura que se genera
            elif action == 'search_clients': #accion de buscar clientes
                data = []
                term = request.POST['term']
                #objetos del modelo de base de datos cliente la busqueda se realizara con name / term es lo que teclea la persona
                clients = Client.objects.filter(
                    #realiza las busquedas del cliente la letra q se usa por que django la usa para poder usar el or |
                    Q(names__icontains=term) | Q(surnames__icontains=term) | Q(dni__icontains=term))[0:10] #muestra 10 clientes referenciados con nombres apellidos y cedula
                for i in clients:
                    item = i.toJSON()
                    # i.get_full_name trae los nombres y apellidos del cliente concatenados desde el modelo
                    item['text'] = i.get_full_name()
                    data.append(item)
            elif action == 'create_client': #accion para crear cliente en el formulario de ventas
                #validacion de transaccion evita que se almacenen datos vacios e incompletos
                with transaction.atomic():
                    #enviando la informacion para almacenarla
                    frmClient = ClientForm(request.POST)
                    #almacenanado la informacion del cliente
                    data = frmClient.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)  # safe=False se usa por que estamos trabajando con datos a manera de doccionario

    # metodo context data para enviar parametros adicinales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['det'] = []
        context['frmClient'] = ClientForm()  # variable adicional para enviar el formulario de agregar nuevo cliente
        return context
# clase para editar la factura
class SaleUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Sale
    form_class = SaleForm
    template_name = 'sale/create.html'
    success_url = reverse_lazy('erp:sale_list')
    permission_required = 'change_sale'  # asiganando el permiso para actualizar datos de la tabla
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    #-------Sobreescribiendo el metodo get form que contiene la instancia actual del formulario  -------------------------------------------
    def get_form(self, form_class=None):
        #estos cambios spon para el editar cliente, que los datos coincidan con la edicion
        # variable instence que tendra el objeto actual /datos del formulario
        instance = self.get_object()
        # y le va a pasar el objeto actual a form
        form = SaleForm(instance=instance)
        # enviando el listado que necesita del cliente
        form.fields['cli'].queryset = Client.objects.filter(id=instance.cli.id)
        #retorna los datos actuales del formulario de ventas
        return form
    #--------------------------------------------------
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            #acciones
            if action == 'search_products':  # la accion es busqueda de productos
                data = []  # data es un array
                # conviertes los id de productos seleccionados de string a un listado
                ids_exclude = json.loads(request.POST['ids'])
                # lOGICA DE LA BUSQUEDA CON IMPUT Y MODAL / SI TERM TIENE TEXTO EL MODAL MUIESTRA PRODUCTOS CON ESE TEXTO SI NO EL MODAL MUESTRA TODOS LOS PRODUCTOS EXISTENTES
                # valores que llegan de forms.js
                term = request.POST['term'].strip()
                # Validacion de que el stock sea mayor a 0
                products = Product.objects.filter(stock__gt=0)
                # Validacion para filtrar por term - Si la busquea contiene informacion entonces
                if len(term):
                    # realiza la bsuqueda  si term contiene un texto de busqueda por el filtro de la informacion term
                    # solo muestra productos que estan en stock
                    products = products.filter(name__icontains=term, stock__gt=0)
                # si llega bacio term entonces  va a recorrer toda la informacion de productos
                # toma los productos identifica en la buqueda principal de productos en ventas los id y excluiye los productos que coinciden con el id almacenado en el listado ids_exclude
                for i in products.exclude(id__in=ids_exclude):
                    item = i.toJSON()
                    item['value'] = i.name
                    # item['text'] = i.name
                    data.append(item)
            elif action == 'edit':
                # -------------------------Proceso para cambiar o editar la informacion obtenida y almacenarla--------------
                with transaction.atomic():
                    # -------datos de la tabla derecha del template ------
                    vents = json.loads(request.POST['vents'])
                    # sale = Sale.objects.get(pk=self.get_object().id)
                    sale = self.get_object()
                    sale.date_joined = vents['date_joined']
                    sale.cli_id = vents['cli']
                    sale.subtotal = float(vents['subtotal'])
                    sale.iva = float(vents['iva'])
                    sale.total = float(vents['total'])
                    sale.save()
                    sale.detsale_set.all().delete()  # elimina la data anterior del detalle de la venta para evitar dupliciades
                    # se iteran de nuevo los productos --datos de la tabla izquierda del template
                    for i in vents['products']:
                        det = DetSale()
                        det.sale_id = sale.id
                        det.prod_id = i['id']
                        det.cant = int(i['cant'])
                        det.price = float(i['pvp'])
                        det.subtotal = float(i['subtotal'])
                        det.save()
                        # haciendo el descuento de productos del stock a la hora de vender
                        det.prod.stock -= det.cant  # el stock va a hacer menor o igual a la cantidad que vendio en su momento
                        # almacena ese cambio en la tabla
                        det.prod.save()
                        data = {'id': sale.id}  # diccionario que envia con Jresponse la clave primaria de la factura que se genera
                # ---------------------------------------------------------------------------------
            elif action == 'search_clients': #accion de buscar clientes
                data = []
                term = request.POST['term']
                #objetos del modelo de base de datos cliente la busqueda se realizara con name / term es lo que teclea la persona
                clients = Client.objects.filter(
                    #realiza las busquedas del cliente la letra q se usa por que django la usa para poder usar el or |
                    Q(names__icontains=term) | Q(surnames__icontains=term) | Q(dni__icontains=term))[0:10] #muestra 10 clientes referenciados con nombres apellidos y cedula
                for i in clients:
                    item = i.toJSON()
                    # i.get_full_name trae los nombres y apellidos del cliente concatenados desde el modelo
                    item['text'] = i.get_full_name()
                    data.append(item)
            elif action == 'create_client': #accion para crear cliente en el formulario de ventas
                #validacion de transaccion evita que se almacenen datos vacios e incompletos
                with transaction.atomic():
                    #enviando la informacion para almacenarla
                    frmClient = ClientForm(request.POST)
                    #almacenanado la informacion del cliente
                    data = frmClient.save()

            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    # metodo que trae el detalle de los productos para modificarlos
    def get_details_product(self):
        data = []  # array data
        # control de errores
        try:
            # iterando los detalles y trae los obtejos de datelle de venta pero se hace un filtro por id
            for i in DetSale.objects.filter(sale_id=self.get_object().id):
                item = i.prod.toJSON()  # creando variablke auxiliar  para traer el producto
                item['cant'] = i.cant  # envia la cantidad modificada
                data.append(item)  # llenando de nueva informacion al diccionario array data
        except:
            pass
        return data  # retornando la informacion en una variable

    # parametros adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'edit'  # identificador
        # json.dumps convierte los datos a tipo json diccionario de datos para usar en lenguaje js para evitar conflictos con datos none o vacios
        context['det'] = json.dumps(
            self.get_details_product())  # envia los datos extraidos por el metodo get_details_product para mostrarlos en el template
        context['frmClient'] = ClientForm()  # variable adicional para enviar el formulario de agregar nuevo cliente
        return context
# clase para redireccionar al cancelar el listado de productos de la venta
class SaleDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Sale
    template_name = 'sale/delete.html'
    success_url = reverse_lazy('erp:sale_list')
    permission_required = 'delete_sale'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        return context
# vista para el reporte de la factura en pdf
class SaleInvoicePdfView(View):
 #funcion que permite mostrar imagenes en la factura usando la url estatica de settings.py
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # url para la imagen
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
         #control de errores
        try:
            template = get_template('sale/invoice.html')  #devuelve un html
            #parametros de la factura
            context = { #obtiene los objetos para mostrar en la factura valores etc
                'sale': Sale.objects.get(pk=self.kwargs['pk']),
                #titulos de la factura
                'comp': {'name': 'SystemasPos S.A.', 'ruc': '1118548974', 'Direccion': 'Casanare, Colombia'},
                #icono de la factura
                'icon': '{}{}'.format(settings.MEDIA_URL, 'logo.png') # URL absoluta donde esta el logo imagen de la factura
            }
            html = template.render(context) #permite incrustar paramaetros a la plantilla - le enviamos los datos del diccionario
            response = HttpResponse(content_type='application/pdf')  #devuelve un objeto de httpresponse sera pdf y se descargara
            #permite descargar el pdf añ pc
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
             #objeto que se va a delover crear el pdf  dentro de una variable pisastatus
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
            #si hay un error no imprima nada y redirija a la venta
            pass
        return HttpResponseRedirect(reverse_lazy('erp:sale_list')) #retorna a la lista de ventas

