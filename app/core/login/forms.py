from django import forms #llamando librerias de ofrmulario para poder trabajar
#componenetes de formulario para resetear contraseña
from core.user.models import User #importando el modelo user
#formulario para verificar el username nombre de usuario
class ResetPasswordForm(forms.Form):
    #Componenetes
    #username es un campo textinput  / widget=forms.TextInput permite personalizar el campo
    username = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Ingrese un username',
        'class': 'form-control',
        'autocomplete': 'off'
    }))

# sobreescriiendo metodo clean para validar los datos de formularios en este caso para resetear username de contrase;a
    def clean(self):
        cleaned = super().clean()
        # valida la existencia del usuario & si el user name es igual al que ya existe envia para el mensaje de reseteo
        if not User.objects.filter(username=cleaned['username']).exists():
            # self._errors['error'] = self._errors.get('error', self.error_class())
            # self._errors['error'].append('El usuario no existe')
            # si el user no existe envie mensaje de error
            raise forms.ValidationError('El usuario no existe')
        return cleaned
  #obteniendo datos del usuario que esta realizando la peticion de reinicio de contraseña
    def get_user(self):
        #recuperando con clean data el valor de username  capturando el valor
        username = self.cleaned_data.get('username')
        #retornando el usuario al que pertenece ese uername
        return User.objects.get(username=username)
#formulario para reinicio de co traseña
class ChangePasswordForm(forms.Form):
    #Componenetes
    #username es un campo textinput  / widget=forms.TextInput permite personalizar el campo
    #componentes del formulario
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Ingrese una contraseña',
        'class': 'form-control',
        'autocomplete': 'off'
    }))
    confirmPassword = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Repita la contraseña',
        'class': 'form-control',
        'autocomplete': 'off'
    }))
    #sobreescribiendo metodo celan para validar la contraseña nueva 
    def clean(self):
        cleaned = super().clean()
        #obteniendo la contraseña nueva y la confuirmacion de la contraseña
        password = cleaned['password']
        confirmPassword = cleaned['confirmPassword']
        #validando si el password nuevo  es diferente al de la confirmacion y enviar error
        if password != confirmPassword:
            # self._errors['error'] = self._errors.get('error', self.error_class())
            # self._errors['error'].append('El usuario no existe')
            raise forms.ValidationError('Las contraseñas deben ser iguales')
        return cleaned

