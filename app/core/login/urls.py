from django.urls import path

from core.login.views import *

urlpatterns = [
    #entrar al sistema con login
    path('', LoginFormView.as_view(), name='login'),
    # cerrar sesion
    path('logout/', LogoutView.as_view(), name='logout'),
    #confirmacion de user name para enviar correo electronico e iniciar el proseso de reinicio de contraseña contraseña
    path('reset/password/', ResetPasswordView.as_view(), name='reset_password'),
    # contraseña nueva y confirmacion de la misma <str:token> es la variable secreta para hacer el cambio
    path('change/password/<str:token>/', ChangePasswordView.as_view(), name='change_password')
]
