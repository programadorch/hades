import uuid

from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect, JsonResponse  # envia la respuesta de los metodos post
from django.shortcuts import redirect  # redirige  a una url especificA
from django.urls import reverse_lazy  # retorna a url
from django.utils.decorators import method_decorator  # decorador para modificar vistas
from django.views.decorators.csrf import csrf_exempt  # decorador para modificar vistas
from django.views.generic import FormView, RedirectView
import config.settings as setting  # importa el contenido de setting.py dentro de la carpeta config
from core.login.forms import ResetPasswordForm, ChangePasswordForm  # implementando archivo forms para usar el formulario
# librerias y funciones para envio de emails
import smtplib
from email.mime.multipart import MIMEMultipart  # funcion propia de mime para enviar objetos html por correo electronico
from email.mime.text import MIMEText
from core.user.models import User  # importando el modelo de base de datos
from django.template.loader import render_to_string  # funcion para enviar un template como parametro
from config import settings  # importando setting de la carptea config para usar variables globales de envio de emails



class LoginFormView(LoginView):
    template_name = 'login/login.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(setting.LOGIN_REDIRECT_URL)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Iniciar sesión'
        return context


class LoginFormView2(FormView):
    form_class = AuthenticationForm
    template_name = 'login/login.html'
    success_url = reverse_lazy(setting.LOGIN_REDIRECT_URL)

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.success_url)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Iniciar sesión'
        return context


# -------controlador para cerrar sesion--------------
class LogoutView(RedirectView):
    pattern_name = 'login'

    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return super().dispatch(request, *args, **kwargs)


# --------vista para formulario de confirmacion de username para verificar la autenticidad y exitencia del usuario
class ResetPasswordView(FormView):
    # formulario a trabajar
    form_class = ResetPasswordForm
    # platilla a trabajar
    template_name = 'login/resetpwd.html'
    # url de exito
    success_url = reverse_lazy(setting.LOGIN_REDIRECT_URL)

    # deshabilitando proteccion de las vistas para hacer modificaciones en especuial para el metodo post / permite sobreescribir las vistas
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        pass
        return HttpResponseRedirect(self.success_url)

    # /////////funcion para enviar correo electronico ---- user   envia como parametros el correo electronico del usuario///////////
    def send_email_reset_pwd(self, user):
        #diccionario de datos para dar una respuesta
        data = {}
        # control de errores por si no fun ciona el codigo
        try:
            # --obteniendo la url de dominio ----validando di debug no esta en produccion obtenga la url local ---------------------------
            URL = settings.DOMAIN if not settings.DEBUG else self.request.META['HTTP_HOST']
            # cuando vaya a enviar el correo envie genere el codigo secreto y almacenelo en  token
            user.token = uuid.uuid4()

            user.save()
            # -------------------------------------------
            # estableciendo la conexion con el servidor email Gmail

            mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
            # establece una conexion segura
            mailServer.starttls()
            # llamando las variables globales de settings
            mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
            # correo electronico del usuario confirmado
            email_to = user.email
            # Construimos el mensaje simple / MIMEText es la funcion que arma todos los qrequisitos para el mesnaje
            mensaje = MIMEMultipart()
            # variable global de settins que tine el servidor de correo electronico
            mensaje['From'] = settings.EMAIL_HOST_USER  # a que persona se le enviara el mensaje?
            # correo electronico de destriantario
            mensaje['To'] = email_to
            # asunto del correo
            mensaje['Subject'] = "Reinicio de password Systemaspos.com"
            # render_to_string funcion para enviar un template como parametro / adicional ala plantilla se le esta enviando el usuario para mostrar el nombre
            # apuntando a send_email.html
            content = render_to_string('login/send_email.html', {
                # Variables llamadas desde send_email.html
                'user': user, #envia el usuario a el template o correo electornico
                #enviando la url  obtenida de manera local o produccion y se concatena con el token secreto del usuario generado cuando envio la solicitud de cambio de contraseña
                'link_resetpwd': 'http://{}/login/change/password/{}/'.format(URL, str(user.token)), #url que estara alojada en el boton actualizar del correo electronico
                #enlace de la pagina principal
                'link_home': 'http://{}'.format(URL)
            })
            # agregando la variable content que es el contenido html y el usuario al correo todo tipo string
            mensaje.attach(MIMEText(content, 'html'))
            mailServer.sendmail(settings.EMAIL_HOST_USER,
                                email_to,
                                mensaje.as_string())
        except Exception as e:
            data['error'] = str(e)
            #retornando la respuesta
        return data

    # sobreescritura del metodo post para el envio de informacion
    def post(self, request, *args, **kwargs):
        # diccionario de datos llamado data
        data = {}
        # control de errores
        try:
            # llamando el al formulario de trabajo
            form = ResetPasswordForm(request.POST)  # self.get_form()
            # validar la informacion que llega del formulario atravez del metodo clean en forms.py
            #si los datos del formulario son validos osea el username entonces envia correo electronico
            if form.is_valid():
                #extrallendo los datos de suario atravez de get_user funcion de forms.py
                user = form.get_user()
                #Orden para enviar el correo electronico atravez del metodo send_email y self
                data = self.send_email_reset_pwd(user)
            else:
                # retorna los errores dentro del diccionario llamado data atravez de la variable error la que captura esos datos y lo envia a forms.py
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
            # respuesta
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reseteo de Contraseña'
        return context
#---------vista para el formulario de contraseña nueva y confirmacion de la misma --------------
class ChangePasswordView(FormView):
    # formulario a trabajar
    form_class = ChangePasswordForm
    # platilla a trabajar
    template_name = 'login/chagepwd.html'
    # url de exito
    success_url = reverse_lazy(setting.LOGIN_REDIRECT_URL)

    # deshabilitando proteccion de las vistas para hacer modificaciones en especuial para el metodo post / permite sobreescribir las vistas
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    #sobreescribiendo metodo get para validar el token generado
    def get(self, request, *args, **kwargs):
        #obteniendo token para validar
        token = self.kwargs['token']
        #validacion del token / si un susuario tiene esa variable
        if User.objects.filter(token=token).exists():
            #puede seguir a restaurar contraseña
            return super().get(request, *args, **kwargs)
        #si el usuario no tiene la variable token entonces lo redirecciona a pagina principal
        return HttpResponseRedirect('/')
    # sobreescritura del metodo post para el envio de informacion a la BD
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            #creando instancia del formulario de cragar contraseña y capturando los datos del formulario
            form = ChangePasswordForm(request.POST)
            #valida la informacion de las contraseñas nuevas
            if form.is_valid():
                #Enviando los datos nuevos a la tabla usuario  de la BD
                #instancia de usuario y como llave primaria usar el token
                user = User.objects.get(token=self.kwargs['token'])
                #pasando el pasword nuevo
                user.set_password(request.POST['password'])
                #Actualiza el token
                user.token = uuid.uuid4()
                # guarda los datos en la BD
                user.save()
            else:
                #si es incorrecto enviamos los errores del formulario  por medio de data
                data['error'] =  form.errors
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)
    #variables adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Reseteo de Contraseña'
        context['login_url'] = settings.LOGIN_URL #parametro de variable global para redirigir a login se usa en el html
        return context
