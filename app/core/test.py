from config.wsgi import *
import smtplib
from email.mime.multipart import MIMEMultipart #funcion propia de mime para enviar objetos html por correo electronico
from email.mime.text import MIMEText
from core.user.models import User #importando el modelo de base de datos
from django.template.loader import render_to_string #funcion para enviar un template como parametro

from config import settings

#importando variables globales
# funcion para enviar correos electronicos



def send_email():
    #control de errores por si no fun ciona el codigo
    try:
        #estableciendo la conexion con el servidor email Gmail


        mailServer = smtplib.SMTP(settings.EMAIL_HOST, settings.EMAIL_PORT)
        print(mailServer.ehlo())
        #establece una conexion segura
        mailServer.starttls()
        print(mailServer.ehlo())
        #llamando las variables globales de settings
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print('Conectado..')
        #correo electronico
        email_to = 'programadorch@gmail.com'
        # Construimos el mensaje simple / MIMEText es la funcion que arma todos los qrequisitos para el mesnaje
        mensaje = MIMEMultipart()
        #variable global de settins que tine el servidor de correo electronico
        mensaje['From'] = settings.EMAIL_HOST_USER #a que persona se le enviara el mensaje?
        #correo electronico de destriantario
        mensaje['To'] = email_to
        #asunto del correo
        mensaje['Subject'] = "Correo Automatico enviado por Systemaspos.com"
        #render_to_string funcion para enviar un template como parametro / adicional ala plantilla se le esta enviando el usuario para mostrar el nombre
        content = render_to_string('send_email.html', {'user': User.objects.get(pk=1)})
       #agregando la variable content que es el contenido html y el usuario al correo todo tipo string
        mensaje.attach(MIMEText(content, 'html'))

        #variables globales para el envio del correo , con la funcion sendmail le enviamos el servidor de correo, el correo y contraseña
        mailServer.sendmail(settings.EMAIL_HOST_USER,
                            email_to,
                            mensaje.as_string()) #as_string obtiene todo el codigo del mensaje para enviar

        print('Colegas si te llego este mensaje es por que la evolucion de nuestro aprendizaje es muy buena y vamos en el video 91 ya casi terminando el primer curso django att Ingeniero CARLOS COGUA')
    except Exception as e:
        print(e)

#llamando la funcion
send_email()
