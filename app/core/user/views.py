from django.contrib.auth import update_session_auth_hash #importando librerias para la propiedad de continuar sesion pese a actualizacion de contraseña
from django.contrib.auth.forms import PasswordChangeForm #formulario por defecto de django para editar contaseña
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy #retorna a urls
# metodo de defensa de las vistas django
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
# importacion de vistas genericas de dejango listado de usaurios, crear usuario, editar usuario
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
#importaciones para usar la clase de activacion y seleccion  de rol o grupo durante la sesion activa
from django.contrib.auth.models import Group
from django.views import View
from django.http import JsonResponse, HttpResponseRedirect

from core.erp.mixins import ValidatePermissionRequiredMixin
# modelo de la bd de usuarios
from core.user.models import User
# importando formulario de agregar usuario
from core.user.forms import UserForm
# importando formulario de edicion de usuario logueado
from core.user.forms import UserProfileForm
#---------------------vista de listado de usuarios--------------------------------
class UserListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    # modelo de la bd
    model = User
    # plantilla para trabajar
    template_name = 'user/list.html'
    # permisos
    permission_required = 'view_user'
    # decorador para desactivar la modificacion de vistas de django
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    # sobreescirtura del metodo post
    def post(self, request, *args, **kwargs):
        data = {}
        # control de errores
        try:
            # accion para poder hacer comparaciones en if obteniendo la variable action con post
            action = request.POST['action']
            # accion para buscar
            if action == 'searchdata':
                # diccionario de datos
                data = []
                # iterando los usuarios de la bd
                for i in User.objects.all():
                    # obteniendo datos de la BD con el metodo toJOSON del modelo
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
            # retornando datos
        return JsonResponse(data, safe=False)
    # sobrrescribiendo metodo context data para enviar parametros adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Usuarios'
        # ruta absoluta para crear usuarios en el boton del formulario agregar usuarios
        context['create_url'] = reverse_lazy('user:user_create')
        # ruta absoluta para visualizar listado de usuarios
        context['list_url'] = reverse_lazy('user:user_list')
        context['entity'] = 'Usuarios'
        return context
#---------------------vista para crear usuarios -----------------------------------------
class UserCreateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    # base de datos
    model = User
    # formulario para agregar usuarios en forms.py
    form_class = UserForm
    # plantilla html para usar
    template_name = 'user/create.html'
    # url de retorno de exito
    success_url = reverse_lazy('user:user_list')
    # perimso que corresponde a la creacion de usuarios
    permission_required = 'add_user'
    # url de accion efectiva
    url_redirect = success_url
 # desactivando mecanismo de desfensa de la vista
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    # enviamos los valores a manera de dciionario con el metodo pos sobreescribiendolo
    def post(self, request, *args, **kwargs):
        data = {}
        # control de errores
        try:
            # accion requerida por el metodo post
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                # guardando datos en el formulario
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
            # retornando los datos
        return JsonResponse(data)
 # soreescribiendo metodo para enviar parametros adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # titulo del formulario
        context['title'] = 'Creación de un Usuario'
        context['entity'] = 'Usuarios'
        context['list_url'] = self.success_url
        # accion crear
        context['action'] = 'add'
        return context
# ------------------Vista para editar usuarios-----------------------
class UserUpdateView(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    # base de datos
    model = User
    # formulario que se usara para la modificacion / es el mismo de agregar usuarios
    form_class = UserForm
    # platilla que usaremos para editar usuarios
    template_name = 'user/create.html'
    # url efectiva
    success_url = reverse_lazy('user:user_list')
    # permiso requerido de editar o change
    permission_required = 'change_user'
    url_redirect = success_url

    # desactivando mecanismo de desfensa de la vista
    def dispatch(self, request, *args, **kwargs):
        # con el comando geto objeto le damos valor al objeto esto se usa solo cuando se esta sobreescrobiendo
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    # enviamos los valores a manera de dciionario con el metodo pos sobreescribiendolo
    def post(self, request, *args, **kwargs):
        data = {}
        # control de errores
        try:
            action = request.POST['action']
            # acciones
            if action == 'edit':
                form = self.get_form()
                # guardando el formulario
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
            # enviando el diccionario de datos del modelo
        return JsonResponse(data)

    # soreescribiendo metodo para enviar parametros adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Usuario'
        context['entity'] = 'Usuarios'
        # url para redirigir despues de editar usuarios osea el listado de usuarios
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context
# --------------------Vista para eliminar usuarios ----------------------------------
class UserDeleteView(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    # modelo
    model = User
    # plantilla html
    template_name = 'user/delete.html'
    # ruta de exito
    success_url = reverse_lazy('user:user_list')
    # permisos
    permission_required = 'delete_user'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        # asignando la isntancia del objeto a delete view
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de un Usuario'
        context['entity'] = 'Usuarios'
        context['list_url'] = self.success_url
        return context
#--------------Vista de rol o grupo  seleccionado durante la sesion ---------------------
class UserChangeGroup(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        #recibiendo el valor de la seleccion
        try:
            #obtiene el valor del grupo o rol seleccionado por el usuario
            #  request.session  ['group'] sesion que se esta seleccionando y   Group.objects.get(pk=self.kwargs['pk'] almacenando la sesion
            request.session['group'] = Group.objects.get(pk=self.kwargs['pk'])
        except:
            pass
        #retorne ala ruta absoluta del  dasboard
        return HttpResponseRedirect(reverse_lazy('erp:dashboard'))
#-----------Vista para el formulario de editar Usuario legueado actual ------------------
class UserProfileView(LoginRequiredMixin, UpdateView):
    # modelo base deatos con la que se va a trabajar
    model = User
    # formulario con el que se va a trabajar
    form_class = UserProfileForm
    # plantilla html con la que se va atrabajar
    template_name = 'user/profile.html'
    # url de exito osea cuabdo se almacena lka informa redirigira a el dashboard
    success_url = reverse_lazy('erp:dashboard')
    # desactivando el metodo de seuridad de django para modificar metodo post
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)
    # sobreescribiendo metodo object  para que se edite la instancia actual del usuario logueado
    def get_object(self, queryset=None):
        return self.request.user

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)
 # Variables adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de Perfil'
        context['entity'] = 'Perfil'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context
#-------------Vista para editar contraseña de usuario logueado in afectar la sesion actual----
class UserChangePasswordView(LoginRequiredMixin, FormView):
    model = User
    # PasswordChangeForm fprmulario por defacult de django para editar contraseña actual de usuario
    form_class = PasswordChangeForm
    #platilla html de formulario para editar contraseña
    template_name = 'user/change_password.html'
    #url de exito si es correcto el cambio de conmtraseña redirija al login
    success_url = reverse_lazy('login')

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
# sobreescribiendo el metodo get form para la inicializacion del formulario default de django
    def get_form(self, form_class=None):
        #inicializa con el usuario actual
        form = PasswordChangeForm(user=self.request.user)
        #editando el formulario para mostrar descripciones como marca de agua en los inputs
        form.fields['old_password'].widget.attrs['placeholder'] = 'Ingrese su contraseña actual'
        form.fields['new_password1'].widget.attrs['placeholder'] = 'Ingrese su nueva contraseña'
        form.fields['new_password2'].widget.attrs['placeholder'] = 'Repita su contraseña'
        return form

    def post(self, request, *args, **kwargs):
        data = {}
        #control de errores
        try:
            action = request.POST['action'] #accion requerida
            if action == 'edit':
                #formulario defaul django recibe como parametro del usuario actual seguido de la data de la nueva clave
                form = PasswordChangeForm(user=request.user, data=request.POST)
                if form.is_valid():
                    form.save()
                    #propiedad de django que permite continuar con la sescion actual a pesar de cambiar la contraseña
                    update_session_auth_hash(request, form.user)
                    #si el formulario no es correcto me muestre los errores por defecto de la clase form de django
                else:
                    data['error'] = form.errors
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)
    #parametros adicionales
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de Password'
        context['entity'] = 'Password'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context