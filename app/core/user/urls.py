from django.urls import path
from core.user.views import *
# importando la vista de listado de usuarios
from core.user.views import UserListView
# importando la vista del formulario para crear usuario y editar 
from core.user.views import UserCreateView, UserUpdateView, UserDeleteView,UserChangeGroup
# importando la vista del formulario de editar el usuario actual logueado
from core.user.views import UserProfileView
# importando la vista para editar conseña en sesion actual
from core.user.views import UserChangePasswordView

app_name = 'user'

urlpatterns = [
    # ver listados de usuario
    path('list/', UserListView.as_view(), name='user_list'),
    # crear usuarios nuevos
    path('add/', UserCreateView.as_view(), name='user_create'),
    # editar usuario
    path('update/<int:pk>/', UserUpdateView.as_view(), name='user_update'),
    # eliminar usuarios
    path('delete/<int:pk>/', UserDeleteView.as_view(), name='user_delete'),
    # visualizar rol seleccionado o grupo durante la secion
    path('change/group/<int:pk>/', UserChangeGroup.as_view(), name='user_change_group'),
    # Ver el formulario para editar datos del usuario logueado
    path('profile/', UserProfileView.as_view(), name='user_profile'),
    #url para editar la contraseña del logueado actual con el formulario de django por default
    path('change/password/', UserChangePasswordView.as_view(), name='user_change_password'),
]
