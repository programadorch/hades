from django.forms import *
from django import forms
# importando el modelo de la bd
from core.user.models import User

# ---------------------formulario para agregar usuario///////////////////
class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # el foco va estar en el campo de primer nombre
        self.fields['first_name'].widget.attrs['autofocus'] = True
    # componenetes del formulario
    class Meta:
        # basde de datos a usar
        model = User
        # ordena los componenetes el el orden especificado para el formulario /incluyendo el rol en el formulario
        fields = 'first_name', 'last_name', 'email', 'username', 'password', 'image','groups'
        # componenetes inputs, cajas de texto label etc
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese sus nombres',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese sus apellidos',
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su email',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su username',
                }
            ),
            # render_value=True es una propiedad de django que  permite visualizar la contrase;a en el input cuando vamos a editarla
            'password': forms.PasswordInput(render_value=True,
                attrs={
                    'placeholder': 'Ingrese su password',
                }
            ),
            # roles de administracion con selesct2
            'groups': forms.SelectMultiple(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%',
                'multiple': 'multiple'
            })
        }
        # campos excluidos de la base de datos que no necesito usar
        exclude = ['user_permissions', 'last_login', 'date_joined', 'is_superuser', 'is_active', 'is_staff']
  # -----------------------metodo guardar -----------------------------------
    def save(self, commit=True):
        data = {}
        form = super()
        # control de errores
        try:
            # FORMA MAS EFECTIVA DE ENCRIPTAR LA CONTRASE;A DE USUARIOS NUEVOS SIN AFECTAR A SUPER USUARIOS
            # valida si la informacion en el formulario es valida
            if form.is_valid():
                # OBTENIEDNO LA CONTRASE;A ACTUIAL
                pwd = self.cleaned_data['password']
                # pausa el guardado de inmformacion y devuelve en una variable el objeto actual
                u = form.save(commit=False) #USUARIO QUE ESTAMOS POR CREAR
                if u.pk is None: #SI LA AVRIABLE O USUARIO NO ESTA CREADO
                    u.set_password(pwd) #LE ENVIAMOS LA CONTRAE;A ACTUAL Y SE ENCRIPTA
                else:
                    # SOLICITA EL USUARIO ACTUAL
                    user = User.objects.get(pk=u.pk)
                    # SI EL PASWORD ES DIFERENTE ENTONCES
                    if user.password != pwd:
                        # ENCRIPTA LA CONTRAE;A
                        u.set_password(pwd)
                        # SE GUARDA EL DATO
                u.save()
                # reicia los grupos seleccionado cuando editamos el perfil de usuarioy quitamos algun grupo
                u.groups.clear()
                # ---------------ALMACENADO de LOS GRUPOS EN LA BASE DE DATOS----------------------------
                    # iteracion de los grupos o reoles para almacenarlos en la bd
                for g in self.cleaned_data['groups']:
                    # gropus propiedad que agrega los grupos que se desean al usuario
                    u.groups.add(g)
                    # u es usuarios
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
#---------------------Formulario para editar al usuario logueado actualmente---aqui no se usan los grupos en widgest-------
class UserProfileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['autofocus'] = True

    class Meta:
        model = User
        fields = 'first_name', 'last_name', 'email', 'username', 'password', 'image'
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese sus nombres',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese sus apellidos',
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su email',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su username',
                }
            ),
            'password': forms.PasswordInput(render_value=True,
                                            attrs={
                                                'placeholder': 'Ingrese su password',
                                            }
                                            ),
        }
        exclude = ['user_permissions', 'last_login', 'date_joined', 'is_superuser', 'is_active', 'is_staff', 'groups']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                pwd = self.cleaned_data['password']
                u = form.save(commit=False)
                if u.pk is None:
                    u.set_password(pwd)
                else:
                    user = User.objects.get(pk=u.pk)
                    if user.password != pwd:
                        u.set_password(pwd)
                u.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data
