// inicilizador el listado desplegable de roles en el formulario
$(function () {
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es',
        placeholder: 'Buscar..'
    });
});