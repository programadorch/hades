// implementacion de datattable mediante ajax con el metodo post
$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        // datos de las columnas de la tabla
        columns: [
            {"data": "id"},
            {"data": "full_name"},  //mostrando el nombre completo del usuario
            {"data": "username"},
            {"data": "date_joined"},
            {"data": "image"},
            {"data": "groups"}, //agregando la iteracion de los datos reales de grupos
            {"data": "id"},
        ],
        columnDefs: [
            {
                 // retornando imagen de usuario
                targets: [-3],
                class: 'text-center',
                orderable: false,
                // estilos de las imagenes
                render: function (data, type, row) {
                    return '<img src="'+row.image+'" class="img-fluid mx-auto d-block" style="width: 20px; height: 20px;">';
                }
            },
            /** ------------informacion sobre roles o grupos----------------- **/
            {
                targets: [-2],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                   /** iterando cada valor de los grupos y concatenando en un spam html para mostrar en el template **/
                    var html = '';
                    $.each(row.groups, function (key, value) {
                        html += '<span class="badge badge-success">' + value.name + '</span> ';
                    });
                    return html; /** presentando **/
                }
            },

            {   // botones de accion & como eliminar usuario actualizar usuario
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/user/update/' + row.id + '/" class="btn btn-warning btn-xs btn-flat"><i class="fas fa-edit"></i></a> ';
                    // asignando ruta al boton de eliminar
                    buttons += '<a href="/user/delete/' + row.id + '/" type="button" class="btn btn-danger btn-xs btn-flat"><i class="fas fa-trash-alt"></i></a>';
                    return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
});