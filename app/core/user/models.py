from crum import get_current_request
from django.contrib.auth.models import AbstractUser
from django.db import models

from config.settings import MEDIA_URL, STATIC_URL
from django.forms import model_to_dict


class User(AbstractUser):
    image = models.ImageField(upload_to='users/%Y/%m/%d', null=True, blank=True)
    #  creando campo propiedad  UUIDField para encriptar la contraseña nueva / esto se genera cada vez que haga un cambio de contraseña
    token = models.UUIDField(primary_key=False, editable=False, null=True, blank=True)
    # imagenes de usuarios
    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'img/empty.png')
    # metodo json para obtener datos de la Bd y enviarlos en forma de diccionario de datos
    def toJSON(self):
        # permite obtener diccionario apartir del modelo que enviemos
        # model_to_dict es un metodo django que algunos datos no los pasa a diccionario como fechas etc para eso se debe excluir esos campos
        item = model_to_dict(self, exclude=['password', 'user_permissions', 'last_login'])
        #validacion de la ultima fecha del logueo
        if self.last_login:
            item['last_login'] = self.last_login.strftime('%Y-%m-%d')
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        # sobreescribe la imagen de usuario
        item['image'] = self.get_image()
        # etiqueta para obtener el nombre completo del usuario por medio de la presente propiedad
        item['full_name'] = self.get_full_name()
        # iterando y mostrando los grupos o roles que tiene cada usuario
        # {'id': g.id, 'name': g.name} almacena en un diccionario esos datos el id de grupo y el nombre del grupo
        item['groups'] = [{'id': g.id, 'name': g.name}for g in self.groups.all()]
        return item

    # metodo para seleccionar el primer rol por default cuando un usuario se loguea por primera vez
    def get_group_session(self):
        #control de errore
        try:
            #mantiene la sesion tal cual que estamos usando con
            request = get_current_request()
            groups = self.groups.all()
            #validacion de la exitencia de grupo
            if groups.exists():
                # si 'group' no esta en  request.session:
                if 'group' not in request.session:
                    # se activara el primer grupo que tenga el usuario seleccionado
                    request.session['group'] = groups[0]
        except:
            pass
