from django.http import JsonResponse
from django.urls import reverse_lazy #permite obtener ruta absoluta de una url
#importacion de decorador para poder modificar el metodo post
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
#importacion de la vista generica
from django.views.generic import TemplateView
from core.reports.forms import ReportForm #usando el formulario de reporte de ventas
from core.erp.models import Sale  #importando modelo de BD sale o ventas
# vista de reporte de ventas

#importaciones para hacer operaciones matematicas y almacenarlas en variables
from django.db.models.functions import Coalesce
from django.db.models import Sum


class ReportSaleView(TemplateView):
    template_name = 'sale/report.html' #template que vamos a usar
    #decorador y metodo distpach sin para poder hacer cambios en el metodo post
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
        #sobreescribiendo metodo post
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            #si la variable action es  search_report hace todo el siguiente proceso
            if action == 'search_report':
                data = []
                 #manda las variables para los filtros la variable inicial de defecha y la varible final de fecha
                start_date = request.POST.get('start_date', '')
                end_date = request.POST.get('end_date', '')
                #contiene todas la entas que se an realizado
                search = Sale.objects.all()
                 #se ejecuta solo si la fecha de inicio tiene una vlor y en la fecha final
                if len(start_date) and len(end_date):
                    #filtro con fecha de inicio y fin
                    search = search.filter(date_joined__range=[start_date, end_date])
                    #despues de filtrar itera los resultados del metodo tJson
                for s in search:
                    data.append([
                        s.id, #id de venta
                        s.cli.names, #cliente
                        s.date_joined.strftime('%Y-%m-%d'), #fecha de registro
                        format(s.subtotal, '.2f'),
                        format(s.iva, '.2f'),
                        format(s.total, '.2f'),
                    ])
                #variables para realizar la sumatoria de valores de cada columna
                subtotal = search.aggregate(r=Coalesce(Sum('subtotal'), 0)).get('r')
                iva = search.aggregate(r=Coalesce(Sum('iva'), 0)).get('r')
                total = search.aggregate(r=Coalesce(Sum('total'), 0)).get('r')
                #diccionario de datos para enviar los resultados de las sumatorias 
                data.append([
                    'TOTAL',
                    'TOTAL',
                    'TOTAL',
                    format(subtotal, '.2f'),
                    format(iva, '.2f'),
                    format(total, '.2f'),
                ])
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)
        # sobreescribiendo para agregar variables adicionales / self hacereferencia a la clase actual , **kwargs permite aregar informacion adicional
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
         #titulos
        context['title'] = 'Reporte de Ventas'
        context['entity'] = 'Reportes'
        context['list_url'] = reverse_lazy('sale_report') #url efectiva
        context['form'] = ReportForm() #llamando el componenete imput de form
        return context
