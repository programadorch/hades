var date_range = null;
/** variable inicializada nula **/
var date_now = new moment().format('YYYY-MM-DD'); /** variable global contine moment contendra la fecha del dia de hoy **/
/** Funcion para obetener el reporte de las ventas**/
function generate_report() {
    /** variable para enviar parametros **/
    var parameters = {
        'action': 'search_report', /** accion para usar cuando se redirija al metodo post de la vista **/
        'start_date': date_now, /** fecha seleccionada de inicio **/
        'end_date': date_now, /** fecha seleccionada de fin **/
    };
    /** si la vairblae global date rangue contiene un valor  entonces start date tendra valor de fecha de inicio y end_date la fecha final   **/
    if (date_range !== null) {
        parameters['start_date'] = date_range.startDate.format('YYYY-MM-DD');
        parameters['end_date'] = date_range.endDate.format('YYYY-MM-DD');
    }

    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: parameters,
            dataSrc: ""
        }, /** personalizacion de las columnas **/
        order: false,
        paging: false,
        ordering: false,
        info: false,
        // searching: false, habilita y desahabilita el buscador
        dom: 'Bfrtip',
        /** botones para hacer las descargas de informes y sus respectivos estilos**/
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Descargar Excel <i class="fas fa-file-excel"></i>',
                titleAttr: 'Excel',
                className: 'btn btn-success btn-flat btn-xs'
            },
            {
                extend: 'pdfHtml5',
                text: 'Descargar Pdf <i class="fas fa-file-pdf"></i>',
                titleAttr: 'PDF',
                className: 'btn btn-danger btn-flat btn-xs',
                download: 'open',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                customize: function (doc) {
                    doc.styles = {
                        header: {
                            fontSize: 18,
                            bold: true,
                            alignment: 'center'
                        },
                        subheader: {
                            fontSize: 13,
                            bold: true
                        },
                        quote: {
                            italics: true
                        },
                        small: {
                            fontSize: 8
                        },
                        tableHeader: {
                            bold: true,
                            fontSize: 11,
                            color: 'white',
                            fillColor: '#2d4154',
                            alignment: 'center'
                        }
                    };
                    doc.content[1].table.widths = ['20%', '20%', '15%', '15%', '15%', '15%'];
                    doc.content[1].margin = [0, 35, 0, 0];
                    doc.content[1].layout = {};
                    doc['footer'] = (function (page, pages) {
                        return {
                            columns: [
                                {
                                    alignment: 'left',
                                    text: ['Fecha de creación: ', {text: date_now}]
                                },
                                {
                                    alignment: 'right',
                                    text: ['página ', {text: page.toString()}, ' de ', {text: pages.toString()}]
                                }
                            ],
                            margin: 20
                        }
                    });

                }
            }
        ],
        /**  fin de los botones **/
        columnDefs: [
            {
                /** posicion de las columnas **/
                targets: [-1, -2, -3],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseFloat(data).toFixed(2);
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });
}

/** funcion para inicilaizar los rangos de fechas **/
$(function () {
    $('input[name="date_range"]').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD', /** FOrmato de la fecha */
            /** botones de calendario  sobreescrito de la documentacion de date range picker**/
            applyLabel: '<i class="fas fa-chart-pie"></i> Aplicar',
            cancelLabel: '<i class="fas fa-times"></i> Cancelar',
        }
        /**evento de la doc de daterangepick para el boton aceptar del calendario doble  **/
    }).on('apply.daterangepicker', function (ev, picker) {
        date_range = picker;
        /** la variable incializada es picker tiene el valor actual la fecha del dia de hoy**/
        generate_report(); /** llamando metodo genera el reporte **/
        /**evento de la doc de daterangepick para el boton cancelar del calendario doble  **/
    }).on('cancel.daterangepicker', function (ev, picker) {
        /** la variable incializada es picker tiene el valor actual la fecha del dia de hoy**/
        $(this).data('daterangepicker').setStartDate(date_now);
        /** date_now al dar cancelar la fehca se inicializa al dia de hoy**/
        $(this).data('daterangepicker').setEndDate(date_now);
        /** date_now al dar cancelar la fehca se inicializa al dia de hoy**/
        date_range = picker;
        /** la variable incializada es picker tiene el valor actual la fecha del dia de hoy**/
        generate_report();/** llamando metodo genera el reporte **/
    });
    generate_report();/** llamando metodo genera el reporte **/
});