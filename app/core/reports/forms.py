from django.forms import *

#formulario para reportes de ventas
class ReportForm(Form):
    #componente
    date_range = CharField(widget=TextInput(attrs={ #imput donde seleccionamos los intervalos de fechas
        'class': 'form-control',
        'autocomplete': 'off'
    }))
