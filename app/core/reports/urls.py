from django.urls import path #para uso de urls
from core.reports.views import ReportSaleView #importacion de la vista de reporte de venta

urlpatterns = [
    # reporte de venta
    path('sale/', ReportSaleView.as_view(), name='sale_report'),
]