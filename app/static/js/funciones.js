/** Funcion para detectar errores en el envio de informacion modal Categoria nueva**/
/**Mensaje emergente de error  **/
function message_error(obj) {
    var html = ''; /** variable html **/
    /** si el tipo de objeto es igual a un objeto envie la informacion **/
    if (typeof (obj) === 'object') {
        /**alinia el texto emergente a la izquierda */
        html = '<ul style="text-align: left;">';
        $.each(obj, function (key, value) {
            /** envia el objeto **/
            html += '<li>' + key + ': ' + value + '</li>';
        });
        html += '</ul>';
    }
    /** si no es un objeto solo envie el mensaje de error**/
    else {
        html = '<p>' + obj + '</p>';
    }
    /** TExto de error  mensaje emergente **/
    Swal.fire({
        title: 'Ocurrio un error!', /** texto de mensaje **/
        html: html, /**Atributo html **/
        icon: 'error' /** icono del mensaje **/
    });
}

/** -------funcion para alertas de libreria jquery-confirm------mensajes emergentes de confirmacion  quiere editar una categoria? si no?-----------------------------**/
function submit_with_ajax(url, title, content, parameters, callback)  {
    /** algortimo extraido de la documentacion de jquery-gonfirm   https://craftpip.github.io/jquery-confirm/ **/
    $.confirm({
        theme: 'material',
        title: title,
        icon: 'fa fa-info',
        content: content,
        columnClass: 'small',
        typeAnimated: true,
        cancelButtonClass: 'btn-primary',
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            info: {
                text: "Si",
                btnClass: 'btn-primary',
                action: function () {
                    /**$$$$$ ajax cortado de form es la validacion de errores emergentes$$$$$**/
                    $.ajax({
                        url: url, /**window.location.pathname /**-Url de la pagina actual en la que estoy trabajando**/
                        type: 'POST',
                        data: parameters, /** -datos del formulario **/
                        dataType: 'json', /** -tipo de dato enviado**/
                        /**deshabilitando que por default django proese la data y el tipo de dato para ser modificados con el metodo post **/
                        processData: false,
                        contentType: false,
                    }).done(function (data) {
                        console.log(data);
                        /**- Pregunta si la variable data NO tiene errores por medio del metodo hasOwnProperty **/
                        if (!data.hasOwnProperty('error')) {
                            callback(); /** falta comentario **/
                            return false;
                        }
                        /** si el dato esta repetido me mostrara mensaje de error **/
                        /**-SI efectivamente hay errores llama auna funcion  message_error creada en js/funciones.js **/
                        message_error(data.error); /**- Me muestra por consola de inspeccion el ARRAY con el error**/
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }).always(function (data) {

                    });
                    /**$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ **/
                }
            },
            danger: {
                text: "No",
                btnClass: 'btn-red',
                action: function () {

                }
            },
        }
    })
}