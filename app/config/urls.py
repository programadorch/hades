"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from core.homepage.views import IndexView
from core.login.views import *

from django.conf import settings
from django.conf.urls.static import static
#urls principales del proyecto APPS
urlpatterns = [
    path('', IndexView.as_view(), name='index'), #url de pagina principal
    path('login/', include('core.login.urls')), #url de inision de sesion
    path('admin/', admin.site.urls), #url de usuarios
    path('erp/', include('core.erp.urls')),
    path('reports/', include('core.reports.urls')), #url de reportes
    path('user/', include('core.user.urls')), #url general para ir a listado de usuarios
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
